import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario : string = '';
  contraseña: string = '';
  rol : string = '';

  roles : string[];


  constructor() {
    this.roles = [
      'admin',
      'user'
    ]
   }

  ngOnInit(): void {
  }
  login() {
    console.log(this.usuario+" "+this.contraseña+" "+this.rol);
    
  }

}
