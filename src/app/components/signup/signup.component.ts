import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  nombre: string = '';
  usuario: string = '';
  contraseña: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  Registrate() {
    console.log(this.nombre+" " + this.usuario + " " + this.contraseña);
    
  }

}
